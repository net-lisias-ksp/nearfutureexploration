# Near Future Exploration : UNOFFICIAL

This is a unofficial, non authorized repository for **Near Future Exploration** for historical reference and troubleshooting.


## In a Hurry

* [Binaries](https://github.com/net-lisias-ksph/NearFutureExploration/tree/CC-BY-NC-SA-4.0/tree/Archive)
* [Sources](https://github.com/net-lisias-ksph/NearFutureExploration/tree/CC-BY-NC-SA-4.0/tree/Master)
* [Change Log](./CHANGE_LOG.md)


## Description

This pack contains a set of parts to help enhance and improve the KSP probe experience, particularly in the later game with larger probes. You will find:

* **More Probe Cores**: A set of eight new probe cores in medium (1.25m) and larger (1.875m) sizes. 
* **Probe Bus Parts**: Cargo bay-like parts that match the footprints of most probe cores (stock and NFX). Useful for storing batteries, fuel and the like
* **Probe Fuel Tanks**: New multi-fuel probe tanks in stack and radial sizes that are in the vein of the stock Dumpling and Baguette
* **More Direct** and **Relay Antennae**: More antennae that seamlessly fit into the KSP CommNet system to fill in missing ranges and add more interesting options.
* **Reflector Antennae**: A new type of antenna that does nothing on its own, but instead bounces signal from another antenna to amplify its range. Point antenna at a deployed reflector to recieve the bonus. Available in many sizes. 
* **Small Probe Parts**: A few small probe parts (battery, reaction wheel) to fill out the probe range


## License

This AddOn is (C) [Nertea](https://forum.kerbalspaceprogram.com/index.php?/profile/83952-nertea/). All Right Reserved.

This repository is under the claim of the [right to backup](https://info.legalzoom.com/copyright-law-making-personal-copies-22200.html) and should be considered private:

> Copyright law permits you to make one copy of your computer software for the purpose of archiving the software in case it is damaged or lost. In order to make a copy, you must own a valid copy of the software and must destroy or transfer your backup copy if you ever decide to sell, transfer or give away your original valid copy. It is not legal to sell a backup copy of software unless you are selling it along with the original.

I grant you no rights on any artifact on this repository, unless you own yourself the right to use the software and authorizes me to keep backups for you:

> (a) Making of Additional Copy or Adaptation by Owner of Copy. -- Notwithstanding the provisions of section 106, it is not an infringement for the owner of a copy of a computer program to make or authorize the making of another copy or adaptation of that computer program provided:
> 
>> (2) that such new copy or adaptation is for archival purposes only and that all archival copies are destroyed in the event that continued possession of the computer program should cease to be rightful.

[17 USC §117(a)](https://www.law.cornell.edu/uscode/text/17/117)

By using this repository without the required pre-requisites, I hold you liable to copyright infringement.


## References

* [Nertea](https://forum.kerbalspaceprogram.com/index.php?/profile/83952-nertea/)
	+ [KSP Forum](https://forum.kerbalspaceprogram.com/index.php?/topic/155465-*/)
	+ [imgur](https://imgur.com/a/GMjNGox)
	+ [CurseForge](https://www.curseforge.com/kerbal/ksp-mods/near-future-exploration)
	+ [SpaceDock](https://spacedock.info/mod/2305/Near%20Future%20Exploration)
	+ [GitHub](https://github.com/ChrisAdderley/NearFutureExploration/releases)
